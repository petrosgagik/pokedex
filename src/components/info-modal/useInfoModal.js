import { useMemo } from 'react';

const useInfoModal = (modalData) => {
  const viewData = useMemo(() => {
    return {
      id: modalData?.id,
      img: modalData?.sprites?.front_default,
      name: modalData?.species?.name,
      height: modalData?.height,
      experience: modalData?.base_experience,
      weight: modalData?.weight,
      type:
        modalData?.types?.map((item) => ({
          id: item.slot,
          name: item?.type?.name,
        })) || [],
      stats: modalData?.stats
        ? {
            hp: modalData.stats[0].base_stat || -1,
            attack: modalData.stats[1].base_stat || -1,
            defense: modalData.stats[2].base_stat || -1,
            spAttack: modalData.stats[3].base_stat || -1,
            spDefense: modalData.stats[4].base_stat || -1,
            speed: modalData.stats[5].base_stat || -1,
          }
        : {},
    };
  }, [modalData]);
  return { viewData };
};

export default useInfoModal;
