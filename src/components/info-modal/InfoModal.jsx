import { Modal, Progress } from 'antd';
import { bool, func, object } from 'prop-types';
import React from 'react';

import useInfoModal from './useInfoModal';

const InfoModal = ({ opened, onClose, modalData }) => {
  const { viewData } = useInfoModal(modalData);
  return (
    <Modal
      closable
      open={opened}
      footer={null}
      onCancel={onClose}
      className="infoModal"
    >
      <div className="mainInfo">
        <div>
          <img className="imageBig" src={viewData.img} alt="pic" />
        </div>
        <div>
          <div className="stats">
            №:<span>{viewData.id}</span>
          </div>
          <div className="stats">
            Name: <span>{viewData?.name?.toUpperCase()}</span>
          </div>
          <div className="stats">
            Height:<span>{viewData.height} sm</span>
          </div>
          <div className="stats">
            Wieght:<span>{viewData.weight} kg</span>
          </div>
          <div className="stats">
            Base experiense:<span>{viewData.experience}</span>
          </div>
          <div className="types">
            {viewData.type.map((item) => {
              return (
                <div className={`type ${item.name}`} key={item.id}>
                  {item.name}
                </div>
              );
            })}
          </div>
        </div>
      </div>

      <div className="mainStats">
        <Progress
          type="circle"
          percent={(viewData.stats.hp / 255) * 100}
          format={() => `hp: ${viewData.stats.hp}`}
          width={64}
          strokeColor="#00c2b8"
        />
        <Progress
          type="circle"
          percent={(viewData.stats.attack / 255) * 100}
          format={() => `atk: ${viewData.stats.attack}`}
          width={64}
          strokeColor="#00c2b8"
        />
        <Progress
          type="circle"
          percent={(viewData.stats.defense / 255) * 100}
          format={() => `def: ${viewData.stats.defense}`}
          width={64}
          strokeColor="#00c2b8"
        />
        <Progress
          type="circle"
          percent={(viewData.stats.spAttack / 255) * 100}
          format={() => `sp. atk: ${viewData.stats.spAttack}`}
          width={64}
          strokeColor="#00c2b8"
        />
        <Progress
          type="circle"
          percent={(viewData.stats.spDefense / 255) * 100}
          format={() => `sp. def: ${viewData.stats.spDefense}`}
          width={64}
          strokeColor="#00c2b8"
        />
        <Progress
          type="circle"
          percent={(viewData.stats.speed / 255) * 100}
          format={() => `speed: ${viewData.stats.speed}`}
          width={64}
          strokeColor="#00c2b8"
        />
      </div>
    </Modal>
  );
};
InfoModal.propTypes = {
  opened: bool,
  onClose: func,
  modalData: object,
};
export default InfoModal;
