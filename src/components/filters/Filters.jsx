import { Input, Select } from 'antd';
import React from 'react';

import { types } from './constants';
import { tagRender } from './tagRender';
import useFilters from './useFilters';

const { Option } = Select;

const Filters = () => {
  const { onSearch, onSelectChange, selected, onPressEnter } = useFilters();

  return (
    <div className="filters">
      <div className="search">
        <Input.Search onSearch={onSearch} onPressEnter={onPressEnter} />
      </div>
      <div className="types">
        <Select
          mode="multiple"
          showSearch={false}
          tagRender={tagRender}
          allowClear
          onChange={onSelectChange}
          value={selected}
        >
          {types.map(({ label, value }) => (
            <Option value={value} key={value} className="type">
              {label}
            </Option>
          ))}
        </Select>
      </div>
    </div>
  );
};

export default Filters;
