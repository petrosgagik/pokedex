import { Tag } from 'antd';
import React from 'react';

import { colors } from './constants';

export const tagRender = ({ label, value, closable, onClose }) => {
  const onPreventMouseDown = ({ preventDefault, stopPropagation }) => {
    preventDefault();
    stopPropagation();
  };
  return (
    <Tag
      color={colors[value]}
      onMouseDown={onPreventMouseDown}
      closable={closable}
      onClose={onClose}
      style={{ marginRight: 3 }}
    >
      {label}
    </Tag>
  );
};
