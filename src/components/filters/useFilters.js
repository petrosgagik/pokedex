import { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { filterData } from '../../redux/slices/pokemon';

const useFilters = () => {
  const dispatch = useDispatch();

  const { status } = useSelector((store) => store.pokemon);

  const [selected, setSelected] = useState([]);
  const [search, setSearch] = useState('');

  const onSearch = useCallback((value) => {
    setSearch(value);
  }, []);

  const onPressEnter = useCallback((e) => {
    setSearch(e.target.value);
  }, []);

  const onSelectChange = useCallback(
    (selectedValues) => setSelected(selectedValues),
    []
  );

  useEffect(() => {
    if (status === 'success') {
      dispatch(filterData({ search, types: selected }));
    }
  }, [search, selected, status]);

  return { onSearch, onSelectChange, selected, onPressEnter };
};

export default useFilters;
