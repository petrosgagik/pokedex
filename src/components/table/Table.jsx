import { Table as AntTable } from 'antd';
import { array, bool, func, number } from 'prop-types';
import React from 'react';

import useTable from './useTable';

const Table = ({
  data = [],
  columns = [],
  total = 0,
  getData = () => {},
  loading = false,
}) => {
  const { pagination, onChange } = useTable(getData);
  return (
    <div>
      <AntTable
        className="defaultTable"
        dataSource={data}
        columns={columns}
        rowKey="id"
        size="small"
        pagination={{ ...pagination, total }}
        onChange={onChange}
        loading={loading}
      />
    </div>
  );
};

Table.propTypes = {
  data: array,
  columns: array,
  total: number,
  getData: func,
  loading: bool,
};
export default Table;
