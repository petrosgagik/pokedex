export const initialTableState = {
  current: 1,
  defaultPageSize: 10,
  pageSize: 10,
  showSizeChanger: true,
  pageSizeOptions: ['10', '20', '50'],
  position: ['topCenter', 'bottomCenter'],
  responsive: true,
};
