import { useEffect, useState } from 'react';

import { initialTableState } from './constants';

const useTable = (getData) => {
  const [pagination, setPagination] = useState(initialTableState);
  const onChange = (event) => setPagination(event);
  useEffect(() => {
    const { pageSize, current } = pagination;
    const offset = (current - 1) * pageSize;
    const limit = pageSize;
    getData({ offset, limit });
  }, [pagination]);

  return { pagination, onChange };
};

export default useTable;
