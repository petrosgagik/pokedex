import React from 'react';

import Filters from './components/filters/Filters';
import InfoModal from './components/info-modal/InfoModal';
import Table from './components/table/Table';
import useApp from './useApp';

const App = () => {
  const {
    data,
    total,
    status,
    opened,
    modalData,
    pokemonColumns,
    onClose,
    getData,
  } = useApp();
  return (
    <div className="App">
      <div className="container">
        <h1># POKEDEX</h1>
        <Filters />

        <Table
          data={data}
          columns={pokemonColumns}
          total={total}
          getData={getData}
          loading={status === 'pending'}
        />
      </div>

      <InfoModal opened={opened} onClose={onClose} modalData={modalData} />
    </div>
  );
};

export default App;
