import React from 'react';

export const usePokemonColumns = (setModalData, setOpened) => {
  const pokemonColumns = [
    {
      title: 'Image',
      dataIndex: 'image',
      key: 'image',

      render: (_, row) => {
        return (
          <img className="image" src={row.sprites.front_default} alt="pic" />
        );
      },
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      render: (_, row) => {
        return (
          <div
            className="pokemonName"
            onClick={() => {
              setModalData(row);
              setOpened(true);
            }}
          >
            {row.species.name}
          </div>
        );
      },
    },
    {
      title: 'HP',
      dataIndex: 'hp',
      key: 'hp',
      render: (_, row) => {
        return <div>{row.stats[0].base_stat}</div>;
      },
    },
    {
      title: 'Atk',
      dataIndex: 'Attack',
      key: 'Attack',
      render: (_, row) => {
        return <div>{row.stats[1].base_stat}</div>;
      },
    },
    {
      title: 'Def',
      dataIndex: 'Defense',
      key: 'Defense',
      render: (_, row) => {
        return <div>{row.stats[2].base_stat}</div>;
      },
    },
    {
      title: 'Sp. Atk',
      dataIndex: 'Sp. Attack',
      key: 'Sp. Attack',
      render: (_, row) => {
        return <div>{row.stats[3].base_stat}</div>;
      },
    },
    {
      title: 'Sp. Def',
      dataIndex: 'Sp. Defense',
      key: 'Sp. Defense',
      render: (_, row) => {
        return <div>{row.stats[4].base_stat}</div>;
      },
    },
    {
      title: 'Speed',
      dataIndex: 'Speed',
      key: 'Speed',
      render: (_, row) => {
        return <div>{row.stats[4].base_stat}</div>;
      },
    },
  ];

  return pokemonColumns;
};
