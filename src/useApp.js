import { useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { usePokemonColumns } from './hooks/usePokemonColumns';
import { getDataRequest } from './redux/slices/pokemon';

const useApp = () => {
  const dispatch = useDispatch();
  const [opened, setOpened] = useState(false);
  const [modalData, setModalData] = useState({});
  const {
    selectedData: data,
    total,
    status,
  } = useSelector((store) => store.pokemon);
  const getData = ({ offset, limit }) => {
    dispatch(getDataRequest({ offset, limit }));
  };

  const onClose = useCallback(() => {
    setModalData({});
    setOpened(false);
  }, []);
  const pokemonColumns = usePokemonColumns(setModalData, setOpened);

  return {
    data,
    total,
    status,
    opened,
    modalData,
    pokemonColumns,
    onClose,
    getData,
  };
};

export default useApp;
