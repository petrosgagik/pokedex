import { combineReducers, configureStore } from '@reduxjs/toolkit';

import pokemon from './slices/pokemon';

const store = configureStore({
  reducer: combineReducers({
    pokemon: pokemon.reducer,
  }),
});

export default store;
