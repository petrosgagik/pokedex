import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

const initialState = {
  selectedData: [],
  data: [],
  total: 0,
  status: 'idle',
};

export const getDataRequest = createAsyncThunk(
  'pokemon/getData',
  async ({ offset, limit }) => {
    const { results, count: total } = await fetch(
      `https://pokeapi.co/api/v2/pokemon?offset=${offset}&limit=${limit}`
    ).then((res) => res.json());
    const urlArray = results.map((item) => item.url);
    const data = await Promise.all(
      urlArray.map(async (url) => {
        const res = await fetch(url);
        const data = await res.json();
        return data;
      })
    ).then((pokemons) => pokemons);
    return { data, total };
  }
);

const reducers = {
  filterData: (state, { payload = {} }) => {
    const { search = '', types = [] } = payload;
    let filtered;
    if (search) {
      filtered = state.data.filter((item) => {
        return item?.species?.name?.includes(search);
      });
    } else {
      filtered = state.data.map((item) => item);
    }
    if (types.length) {
      filtered = filtered.filter((item) => {
        let match = false;
        item.types?.forEach((type) => {
          if (types.includes(type?.type?.name)) {
            match = true;
          }
        });
        return match;
      });
    }
    state.selectedData = filtered;
  },
};

const pokemon = createSlice({
  name: 'pokemon',
  initialState,
  reducers,
  extraReducers: (builder) => {
    builder.addCase(getDataRequest.fulfilled, (state, action) => {
      const { data, total } = action.payload;
      state.data = data;
      state.total = total;
      state.status = 'success';
    });
    builder.addCase(getDataRequest.pending, (state) => {
      state.status = 'pending';
    });
  },
});

export const { filterData } = pokemon.actions;

export default pokemon;
